package state;

/**
 * 【环境类】
 *
 * @Author: luanxd
 * @Date: 2021-07-08 9:38
 */
public class Context {
    private State state;

    public Context(){
        this.state = new ConcreteStateA();
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void handle(){
        state.handle(this);
    }
}
