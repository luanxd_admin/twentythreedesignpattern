package state;

/**
 * 【具体状态A类】
 *
 * @Author: luanxd
 * @Date: 2021-07-08 9:40
 */
public class ConcreteStateA extends State{
    @Override
    public void handle(Context context) {
        System.err.println("当前状态是 A.");
        context.setState(new ConcreteStateB());
    }
}
