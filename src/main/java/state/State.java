package state;

/**
 * 【抽象状态类】
 *
 * @Author: luanxd
 * @Date: 2021-07-08 9:37
 */
public abstract class State {
    public abstract void handle(Context context);
}
