package state;

/**
 * 【具体状态B类】
 *
 * @Author: luanxd
 * @Date: 2021-07-08 9:40
 */
public class ConcreteStateB extends State{
    @Override
    public void handle(Context context) {
        System.err.println("当前状态是 B.");
        context.setState(new ConcreteStateA());
    }
}
