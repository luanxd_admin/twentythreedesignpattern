package mediator;

import mediator.interf.Colleague;

/**
 * 具体同事类1
 */
public class ConcreteColleague1 extends Colleague {
    @Override
    public void receive() {
        System.err.println("具体同事类1收到请求。");
    }
    @Override
    public void send() {
        System.err.println("具体同事类1发出请求。");
        //请中介者转发
        mediator.relay(this);
    }
}
