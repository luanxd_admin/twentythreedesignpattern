package mediator;

import mediator.interf.Colleague;

/**
 * 具体同事类2
 */
public class ConcreteColleague2 extends Colleague {
    @Override
    public void receive() {
        System.err.println("具体同事类2收到请求。");
    }
    @Override
    public void send() {
        System.err.println("具体同事类2发出请求。");
        //请中介者转发
        mediator.relay(this);
    }
}
