package mediator.interf;

/**
 * 抽象中介者
 */
public abstract class Mediator {
    public abstract void register(Colleague colleague);

    /**
     * 转发
     * @param cl
     */
    public abstract void relay(Colleague cl);
}
