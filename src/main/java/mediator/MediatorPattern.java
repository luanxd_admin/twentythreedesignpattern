package mediator;

import mediator.interf.Colleague;
import mediator.interf.Mediator;

public class MediatorPattern {
    public static void main(String[] args) {
        //具体中介者
        Mediator md = new ConcreteMediator();
        Colleague c1, c2;
        c1 = new ConcreteColleague1();
        c2 = new ConcreteColleague2();
        md.register(c1);
        md.register(c2);
        c1.send();
        System.err.println("-------------");
        c2.send();
    }
}
