package iterator;

import iterator.interf.Aggregate;
import iterator.interf.Iterator;

public class IteratorPattern {
    public static void main(String[] args) {
        Aggregate ag = new ConcreteAggregate();
        ag.add("中山大学");
        ag.add("华南理工");
        ag.add("韶关学院");
        System.err.print("聚合的内容有：");
        Iterator it = ag.getIterator();
        while (it.hasNext()) {
            Object ob = it.next();
            System.err.print(ob.toString() + "\t");
        }
        Object ob = it.first();
        System.err.println("\nFirst：" + ob.toString());
    }
}
