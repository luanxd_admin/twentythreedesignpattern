package visitor;

import visitor.interf.Visitor;

/**
 * 具体访问者B类
 */
public class ConcreteVisitorB implements Visitor {
    public void visit(ConcreteElementA element) {
        System.err.println("具体访问者B访问-->" + element.operationA());
    }
    public void visit(ConcreteElementB element) {
        System.err.println("具体访问者B访问-->" + element.operationB());
    }
}
