package visitor;

import visitor.interf.Visitor;

/**
 * 具体访问者A类
 */
public class ConcreteVisitorA implements Visitor {
    public void visit(ConcreteElementA element) {
        System.err.println("具体访问者A访问-->" + element.operationA());
    }
    public void visit(ConcreteElementB element) {
        System.err.println("具体访问者A访问-->" + element.operationB());
    }
}
