package visitor.interf;

import visitor.interf.Visitor;

//抽象元素类
public interface Element {
    void accept(Visitor visitor);
}
