package visitor.interf;


import visitor.ConcreteElementA;
import visitor.ConcreteElementB;

/**
 * 抽象访问者
 */
public interface Visitor {
    void visit(ConcreteElementA element);
    void visit(ConcreteElementB element);
}
