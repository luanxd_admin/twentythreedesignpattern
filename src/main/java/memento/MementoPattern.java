package memento;

/**
 * 客户类
 */
public class MementoPattern {
    public static void main(String[] args) {
        //发起人
        Originator originator = new Originator();
        //管理者
        Caretaker caretaker = new Caretaker();

        originator.setState("S0");
        System.err.println("初始状态:" + originator.getState());

        //保存状态
        caretaker.setMemento(originator.createMemento());

        originator.setState("S1");
        System.err.println("新的状态:" + originator.getState());

        //恢复状态
        originator.restoreMemento(caretaker.getMemento());
        System.err.println("恢复状态:" + originator.getState());
    }
}
