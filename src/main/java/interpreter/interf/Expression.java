package interpreter.interf;

/**
 * 【抽象表达式接口】
 *
 * @Author: luanxd
 * @Date: 2021-07-06 10:22
 */
public interface Expression {

    /**
     * 解释方法
     * @param info
     */
    boolean interpret(String info);
}
