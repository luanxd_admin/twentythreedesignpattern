package observer;

import observer.interf.Observer;

/**
 * 【具体目标】
 *
 * @Author: luanxd
 * @Date: 2021-07-08 9:24
 */
public class ConcreteSubject extends Subject {
    /**
     * 通知观察者方法
     */
    @Override
    public void notifyObserver() {
        System.err.println("具体目标发生改变...");
        System.err.println("--------------");

        for(Observer obs:observers){
            obs.response();
        }
    }
}
