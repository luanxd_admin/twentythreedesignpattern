package observer.interf;

/**
 * 抽象观察者
 */
public interface Observer {
    /**
     * 返回
     */
    void response();
}
