package observer;

/**
 * 【】
 *
 * @Author: luanxd
 * @Date: 2021-07-08 9:25
 */
public class ObserverPattern {
    public static void main(String[] args) {
        Subject subject = new ConcreteSubject();

        ConcreteObjserver1 concreteObjserver1 = new ConcreteObjserver1();
        ConcreteObjserver1 concreteObjserver2 = new ConcreteObjserver1();

        subject.add(concreteObjserver1);
        subject.add(concreteObjserver2);

        subject.notifyObserver();

    }
}
