package observer;

import observer.interf.Observer;

/**
 * 【具体观察者1】
 *
 * @Author: luanxd
 * @Date: 2021-07-08 9:19
 */
public class ConcreteObjserver2 implements Observer {
    /**
     * 返回
     */
    public void response() {
        System.err.println("具体观察者2作出反应！");
    }
}
